# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#Project file for AnalysisTop
set( PROJECT_NAME "AnalysisTop" )
set( PROJECT_VERSION "2.6.3" )

# Other projects that this one depends on
set( PROJECT_DEPS "AnalysisTopExternals" "2.6.3" )
