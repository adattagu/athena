/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/*
 * IPixelTDAQSvc.h
 *
 * Interface for PixelTDAQSvc
 * Service to get the status of pixel modules from TDAQ
 *
 * georg@cern.ch
 */


#ifndef PIXELCONDITIONSSERVICES_IPIXELTDAQSVC_H
#define PIXELCONDITIONSSERVICES_IPIXELTDAQSVC_H


#include "PixelConditionsTools/IPixelTDAQSvc.h"


#endif
