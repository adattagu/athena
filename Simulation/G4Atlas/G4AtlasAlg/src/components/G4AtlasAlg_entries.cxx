#include "../G4AtlasAlg.h"
#include "../AthenaStackingActionTool.h"
#include "../AthenaTrackingActionTool.h"

DECLARE_COMPONENT( G4AtlasAlg )
DECLARE_COMPONENT( G4UA::AthenaStackingActionTool )
DECLARE_COMPONENT( G4UA::AthenaTrackingActionTool )

