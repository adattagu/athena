#include "TrigT2MinBias/T2MbtsFex.h"
#include "TrigT2MinBias/T2MbtsHypo.h"
#include "TrigT2MinBias/TrigCountSpacePoints.h"
#include "TrigT2MinBias/TrigCountSpacePointsHypo.h"
#include "TrigT2MinBias/TrigCountTrtHits.h"
#include "TrigT2MinBias/TrigCountTrtHitsHypo.h"
#include "TrigT2MinBias/T2ZdcFex.h"
#include "TrigT2MinBias/T2ZdcHypo.h"

DECLARE_COMPONENT( T2MbtsFex )
DECLARE_COMPONENT( T2MbtsHypo )
DECLARE_COMPONENT( TrigCountSpacePoints )
DECLARE_COMPONENT( TrigCountSpacePointsHypo )
DECLARE_COMPONENT( TrigCountTrtHits )
DECLARE_COMPONENT( TrigCountTrtHitsHypo )
DECLARE_COMPONENT( T2ZdcFex )
DECLARE_COMPONENT( T2ZdcHypo )

