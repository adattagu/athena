#include "TrigInDetEvent/TrigInDetTrack.h"
#include "TrigInDetRecoTools/TrigL2PattRecoStrategyA.h"
#include "TrigInDetRecoTools/TrigL2PattRecoStrategyB.h"
#include "TrigInDetRecoTools/TrigL2PattRecoStrategyC.h"
#include "TrigInDetRecoTools/TrigL2PattRecoStrategyF.h"
#include "TrigInDetRecoTools/TrigL2PattRecoStrategyT.h"
#include "TrigInDetRecoTools/TrigL2DupTrackRemovalTool.h"
#include "TrigInDetRecoTools/TrigInDetRoadMakerTool.h"
#include "TrigInDetRecoTools/FTK_DataProviderTool.h"
#include "../TrigL2LayerSetPredictorTool.h"


DECLARE_COMPONENT( TrigL2PattRecoStrategyA )
DECLARE_COMPONENT( TrigL2PattRecoStrategyB )
DECLARE_COMPONENT( TrigL2PattRecoStrategyC )
DECLARE_COMPONENT( TrigL2PattRecoStrategyF )
DECLARE_COMPONENT( TrigL2PattRecoStrategyT )
DECLARE_COMPONENT( TrigL2DupTrackRemovalTool )
DECLARE_COMPONENT( TrigInDetRoadMakerTool )
DECLARE_COMPONENT( FTK_DataProviderTool )
DECLARE_COMPONENT( TrigL2LayerSetPredictorTool )

